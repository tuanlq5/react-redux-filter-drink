//Root reducer(Đây là nơi chứa reducer chính)
import { combineReducers } from "redux"; //thư viện để tạo reducer

import taskReducer from "./task.reducer";

const rootReducer = combineReducers({
    taskReducer
});

export default rootReducer;