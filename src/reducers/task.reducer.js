import { TASK_INPUT_CHANGE, TASK_ADD_CLICK, TASK_TOGGLE_STATUS } from "../constants/task.constants";


const initialState = {
    input: "",
    taskList: [//return state 
        {STT: 1, Drink: "Cà phê", id:"1", STTKey: "12"},
        {STT: 2, Drink: "Trà tắc" , id:"2", STTKey: "22"},
        {STT: 3, Drink: "Pepsi" , id:"3",STTKey: "32"},
        {STT: 4, Drink: "Cocacola" , id:"4",STTKey: "42"},
        {STT: 5, Drink: "Trà sữa" , id:"5", STTKey: "52"},
        {STT: 6, Drink: "Matcha" , id:"6", STTKey: "62"},
        {STT: 7, Drink: "Hồng trà" , id:"7", STTKey: "72"},
        {STT: 8, Drink: "Trà xanh kèm Cheese" , id:"84", STTKey: "82"},
        {STT: 9, Drink: "Trà đá" , id:"9", STTKey: "92"}
    ],
    newlist: [ //imutable state
        {STT: 1, Drink: "Cà phê", id:"1", STTKey: "12"},
        {STT: 2, Drink: "Trà tắc" , id:"2", STTKey: "22"},
        {STT: 3, Drink: "Pepsi" , id:"3",STTKey: "32"},
        {STT: 4, Drink: "Cocacola" , id:"4",STTKey: "42"},
        {STT: 5, Drink: "Trà sữa" , id:"5", STTKey: "52"},
        {STT: 6, Drink: "Matcha" , id:"6", STTKey: "62"},
        {STT: 7, Drink: "Hồng trà" , id:"7", STTKey: "72"},
        {STT: 8, Drink: "Trà xanh kèm Cheese" , id:"84", STTKey: "82"},
        {STT: 9, Drink: "Trà đá" , id:"9", STTKey: "92"}
    ],
    returnList: [ //return state
        
    ],
    cellValue:"",
    cellData: []
}
//Chuyển ngôn ngữ tiếng việt về không dấu và chữ hoa thành chữ thường
function toLowerCaseNonAccentVietnamese(str) {
    str = str.toLowerCase();
//     We can also use this instead of from line 11 to line 17
//     str = str.replace(/\u00E0|\u00E1|\u1EA1|\u1EA3|\u00E3|\u00E2|\u1EA7|\u1EA5|\u1EAD|\u1EA9|\u1EAB|\u0103|\u1EB1|\u1EAF|\u1EB7|\u1EB3|\u1EB5/g, "a");
//     str = str.replace(/\u00E8|\u00E9|\u1EB9|\u1EBB|\u1EBD|\u00EA|\u1EC1|\u1EBF|\u1EC7|\u1EC3|\u1EC5/g, "e");
//     str = str.replace(/\u00EC|\u00ED|\u1ECB|\u1EC9|\u0129/g, "i");
//     str = str.replace(/\u00F2|\u00F3|\u1ECD|\u1ECF|\u00F5|\u00F4|\u1ED3|\u1ED1|\u1ED9|\u1ED5|\u1ED7|\u01A1|\u1EDD|\u1EDB|\u1EE3|\u1EDF|\u1EE1/g, "o");
//     str = str.replace(/\u00F9|\u00FA|\u1EE5|\u1EE7|\u0169|\u01B0|\u1EEB|\u1EE9|\u1EF1|\u1EED|\u1EEF/g, "u");
//     str = str.replace(/\u1EF3|\u00FD|\u1EF5|\u1EF7|\u1EF9/g, "y");
//     str = str.replace(/\u0111/g, "d");
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng 
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
    return str;
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case TASK_INPUT_CHANGE:
            state.input = action.payload;
            break;
        case TASK_ADD_CLICK:
            //return lại data cho taskList 
            state.taskList = state.newlist;  
             //map Drink to lower case array
            state.taskList =  state.taskList.map(e => { 
                return {
                    ...e, Drink:toLowerCaseNonAccentVietnamese(e.Drink)
                }
            }).filter(elements =>   //tìm item trùng ký tự 
                elements.Drink.includes( toLowerCaseNonAccentVietnamese(state.input)));
            //tìm được danh mục ta cần lấy dữ liệu chưa bị thay đổi (2 loop, with spread operator)
            //empty return list before adding data
            state.returnList=[];
            for(var i = 0 ; i < state.newlist.length; i ++){
                for(var j = 0 ; j < state.taskList.length; j++){
                    if(state.taskList[j].STT === state.newlist[i].STT){
                        state.returnList.push(state.newlist[i]);
                    }
                }
            }
            //sau đó tra về taskList có dấu
            state.taskList = state.returnList;
            console.log(state.returnList);
            //nếu input không được nhập thì truyền lại toàn bộ danh sách 
            if(!state.input){
                state.taskList = state.newlist;
            }
            state.input = "";
            break;
        case TASK_TOGGLE_STATUS:
            state.cellValue = action.payload.innerHTML;
            console.log(state.cellValue);
            for(var index = 0;  index < state.newlist.length; index ++){
                if(state.newlist[index].Drink === state.cellValue){
                    state.cellData = state.newlist[index];
                    console.log(state.cellData);
                } 
            }
            break;
        default:
            break;
    }
    return { ...state };
}

export default taskReducer;