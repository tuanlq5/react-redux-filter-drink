import { TextField, Button, Grid, Container } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell , { tableCellClasses }from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { taskToggleHandler, addTaskHandler, inputChangeHandler } from "../action/task.action";

 
const Task = () => {
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();
   
    // B1: Nhận giá trị khởi tạo của state trong giai đoạn mounting
    const {input, taskList} = useSelector((reduxData) => { 
        return (reduxData.taskReducer);
    })
    // B2: Khai báo những actions
    const inputTaskChangeHandler = (event) => {
        dispatch(inputChangeHandler(event.target.value));
    }
    const addClickHandler = (event) => {
        event.preventDefault();//tránh load lại page khi ấn enter
        dispatch(addTaskHandler());
    }
    const toggleClickHandler = (elementId) => {
        dispatch(taskToggleHandler(elementId.target.closest("td")));
    }
    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
          border: 0,
        },
      }));
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
          backgroundColor: theme.palette.common.black,
          color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
          fontSize: 14,
        },
      }));
    return(
        <Container>
            <form>
                <Grid sx={{ m: 5 }} container spacing={0} direction="row" alignItems="center" >
                    <Grid item xs={3} md={3} lg={3} sm={3} >
                        <label>Nhập Nội dung dòng</label>
                    </Grid>
                    <Grid item xs={6} md={6} lg={6} sm={6}>
                        <TextField value={input} id="input-Drink" label="Input task here" fullWidth onChange={inputTaskChangeHandler} > </TextField>
                    </Grid>
                    <Grid  item xs={3} md={3} lg={3} sm={3} textAlign = "center">
                        <Button variant="Contained"  onClick={addClickHandler}  id="btn-filter" type="submit">Filter Row</Button>
                    </Grid>
                </Grid>
                <Grid container>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650}} style={{ tableLayout: 'fixed' }} aria-label="simple table" >
                            <TableHead sx={{backgroundColor: "rgb(43,43,43)"}}>
                                <TableRow>
                                    <TableCell align="center" sx={{color: "white !important"}}>STT</TableCell>
                                    <TableCell align="center" sx={{color: "white !important"}}>Nội dung</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {taskList.map((row, index) => (
                                    <StyledTableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <StyledTableCell  align="center" component="th" scope="row" key={row.STTKey}>
                                            {row.STT}
                                        </StyledTableCell>
                                        <StyledTableCell align="center" key={row.id} onClick={toggleClickHandler} >
                                            {row.Drink}
                                        </StyledTableCell>
                                    </StyledTableRow>
                                    ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </form>
        </Container>
    )
}
export default Task